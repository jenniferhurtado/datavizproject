import datetime
import json
import time

from dateutil.relativedelta import relativedelta
from django.core import serializers
from django.db import connections
from django.db.models import Count
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.template import loader

from .models import Play, TwitterUser, Edge, GenderTwitterUser

MINIMUM_FOLLOWERS = 1400
MINIMUM_FOLLOWERS_STR = "minimum_followers"
FOLLOWERS_RATIO = 5
FOLLOWERS_RATIO_STR = "followers_ratio"

MONTHS_DICTIONARY = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "Seeptember",
    10: "October",
    11: "November",
    12: "December",
}


def index(request):
    template = loader.get_template('dashboard/dashboard_assemble.html')
    influencers = get_influencers_users()

    today = datetime.date(2018, 5, 5)
    begining_of_this_month = datetime.date(today.year, today.month, 1)
    last_month_1 = begining_of_this_month - relativedelta(months=1)
    last_month_2 = begining_of_this_month - relativedelta(months=2)
    last_month_3 = begining_of_this_month - relativedelta(months=3)
    last_month_4 = begining_of_this_month - relativedelta(months=4)

    users_created_month_1 = TwitterUser.objects.filter(creation_date__range=[last_month_1, begining_of_this_month]).count()
    users_created_month_2 = TwitterUser.objects.filter(creation_date__range=[last_month_2, last_month_1]).count()
    users_created_month_3 = TwitterUser.objects.filter(creation_date__range=[last_month_3, last_month_2]).count()
    users_created_month_4 = TwitterUser.objects.filter(creation_date__range=[last_month_4, last_month_3]).count()

    context = {MINIMUM_FOLLOWERS_STR: MINIMUM_FOLLOWERS,
               FOLLOWERS_RATIO_STR: FOLLOWERS_RATIO,
               "ranking_influencers": influencers[:10],
               "users_created_month_1": users_created_month_1,
               "users_created_month_2": users_created_month_2,
               "users_created_month_3": users_created_month_3,
               "users_created_month_4": users_created_month_4,
               "users_created_month_1_name": MONTHS_DICTIONARY[last_month_1.month],
               "users_created_month_2_name": MONTHS_DICTIONARY[last_month_2.month],
               "users_created_month_3_name": MONTHS_DICTIONARY[last_month_3.month],
               "users_created_month_4_name": MONTHS_DICTIONARY[last_month_4.month],
               }
    return HttpResponse(template.render(context, request))


def users_by_year(request):
    template = loader.get_template('dashboard/plot_users_created_by_year.html')
    context = {}
    return HttpResponse(template.render(context, request))


def relationships(request):
    template = loader.get_template('dashboard/relationships.html')
    context = {}
    return HttpResponse(template.render(context, request))


def influencers(request):
    template = loader.get_template('dashboard/influencers.html')
    context = {}
    return HttpResponse(template.render(context, request))


def ranking_influencers(request):
    template = loader.get_template('dashboard/ranking_influencers.html')
    influencers = get_influencers_users()
    context = {"ranking_influencers": influencers[:10]}
    return HttpResponse(template.render(context, request))


def get_influencers_users():
    possible_influencers = TwitterUser.objects.filter(followers__gte=MINIMUM_FOLLOWERS).order_by('-followers')
    influencers = [(user.name, (user.followers / user.following)) for user in possible_influencers]
    influencers.sort(key=lambda tuple: tuple[1], reverse=True)
    influencers = [(_, int(ratio)) for (_, ratio) in influencers]
    return influencers


def graph(request):
    return render(request, 'graph/graph.html')


def forceGraph(request, gt_followers=None, gt_fav=None):
    if gt_followers:
        nodes = TwitterUser.objects.filter(followers__gte=gt_followers)

    elif gt_fav:
        nodes = TwitterUser.objects.filter(favs__gte=gt_fav)
    else:
        nodes = TwitterUser.objects.all()[:600]

    links = Edge.objects.all()

    linksIndexed = []

    for link in links:
        target = None
        source = None
        for i, node in enumerate(nodes):
            if link.retweeter == node:
                target = i
            elif link.retweeted == node:
                source = i
            if target is not None and source is not None:
                break
        if target is not None and source is not None:
            linksIndexed.append({'target': target, 'source': source})
        target = None
        source = None

    template = loader.get_template('dashboard/force-graph.html')
    context = {'nodes': serializers.serialize("json", nodes), 'links': json.dumps(linksIndexed)}

    return HttpResponse(template.render(context, request))


def play_count_by_month(request):
    data = Play.objects.all() \
        .extra(
            select={
                'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')
            }
        ) \
        .values('month') \
        .annotate(count_items=Count('id'))
    return JsonResponse(list(data), safe=False)


def users_created_by_year(request):
    users = TwitterUser.objects.filter(creation_date__isnull=False).order_by('creation_date')
    start_date = users.first().creation_date
    end_date = users.last().creation_date

    end_year = end_date.year
    year = start_date.year

    data = []

    while year <= end_year:
        init_date = datetime.datetime(year, 1, 1)
        finish_date = datetime.datetime(year, 12, 31)
        number_users = TwitterUser.objects.filter(creation_date__range=[init_date, finish_date]).count()
        data.append({'year': year, 'count': number_users})
        year += 1

    return JsonResponse(list(data), safe=False)


def popularity_plot(request):
    template = loader.get_template('dashboard/plot_popularity.html')
    context = {MINIMUM_FOLLOWERS_STR: MINIMUM_FOLLOWERS, FOLLOWERS_RATIO_STR: FOLLOWERS_RATIO}
    return HttpResponse(template.render(context, request))


def get_popularity_data(request):
    users = TwitterUser.objects.filter(following__isnull=False, followers__isnull=False)
    data = []

    for user in users:
        data.append({'following': user.following, 'followers': user.followers, 'name': user.name})

    return JsonResponse(list(data), safe=False)


def user_favorites(request):

    start = time.time()

    relationships = Edge.objects.filter().order_by('-retweeter_id')

    paparazzis =  [None] * (highest_id + 1)

    minimum = int(request.GET.get('minimum')) if request.GET.get('minimum') is not None else 0

    for relation in relationships:

        retweeted = relation.retweeted
        retweeter = relation.retweeter

        # Ha recibido un numero X de retweets mayor al mínimo (Es famosa)
        if retweeted.retweets_received < minimum:
                continue

        if retweeter.identifier in paparazzis:

            if retweeted.identifier in paparazzis[retweeter.identifier]:
                paparazzis[retweeter.identifier][retweeted.identifier] += 1
            else:
                paparazzis[retweeter.identifier][retweeted.identifier] = 1
        else:
            paparazzis[retweeter.identifier] = {}
            paparazzis[retweeter.identifier][retweeted.identifier] = 1

    paparazzis = {ind : x for ind, x in enumerate(paparazzis) if x is not None}

    end = time.time()

    print("Tiempo users favorites", end-start)

    counter = 0
    for k in paparazzis:
        counter+=1
        print(k,paparazzis[k])
        if counter == 10:
            break

    return JsonResponse(paparazzis, safe=False)

def favorite_users(request):

    start = time.time()

    relationships = Edge.objects.filter()

    top_influencers = set()

    for relation in relationships:

        top_influencers.add(relation.retweeted)

    top_influencers = list(top_influencers)

    top_influencers.sort(key=lambda k: k.retweets_received, reverse=True)

    top_influencers = top_influencers[:10]

    top_infl_by_id = {}

    for user in top_influencers:
        user_keys = vars(user)

        try:
            del user_keys["_state"]
        except KeyError:
            pass

        top_infl_by_id[user.identifier] = {}

    top_identifiers = []

    for top in top_influencers:
        top_identifiers.append(top.identifier)

    relationships = Edge.objects.filter(retweeted__identifier__in=top_identifiers)

    for relation in relationships:
        if relation.retweeter.identifier in top_infl_by_id[relation.retweeted.identifier]:
            top_infl_by_id[relation.retweeted.identifier][relation.retweeter.identifier] += 1
        else:
            top_infl_by_id[relation.retweeted.identifier][relation.retweeter.identifier] = {}
            top_infl_by_id[relation.retweeted.identifier][relation.retweeter.identifier] = 1

    end = time.time()

    send_influencers = []
    
    for t in top_infl_by_id:
        temp_dic = {t : top_infl_by_id[t]}
        send_influencers.append(temp_dic)

    print("Tiempo favorite users", end-start)

    return JsonResponse(list(send_influencers), safe=False)

def users_by_id(request):
    users = TwitterUser.objects.filter().order_by('identifier')

    user_dic = {}

    for user in users:
        user_keys = vars(user)

        try:
            del user_keys["_state"]
        except KeyError:
            pass
        user_keys['retweets_performed'] = user.retweets_performed
        user_keys['retweets_received'] = user.retweets_received
        user_dic[user.identifier] = user_keys

    return JsonResponse(user_dic, safe=False)


def genders_of_users(request):
    template = loader.get_template('dashboard/genders_of_users.html')
    context = {}
    return HttpResponse(template.render(context, request))

def get_users_gender_data(request):
    all_users = GenderTwitterUser.objects.filter().order_by('identifier')

    female_users, male_users, brand_users = 0, 0, 0

    for usr in all_users:
        if usr.gender == 1:
            female_users += 1
        elif usr.gender == 0:
            male_users += 1
        else:
            brand_users += 1

    print(male_users)
    print(female_users)
    print(brand_users)
    total_users = male_users + female_users + brand_users
    male_users = round(male_users * 100 / total_users)
    female_users = round(female_users * 100 / total_users)
    brand_users = round(brand_users * 100 / total_users)
    print(male_users)
    print(female_users)
    print(brand_users)

    data = []

    data.append({"label": 'Male', "value": male_users})
    data.append({"label": 'Female', "value": female_users})
    data.append({"label": 'Brand', "value": brand_users})

    return JsonResponse(data, safe=False)

def gender_tweets(request):
    template = loader.get_template('dashboard/gender_tweets.html')
    context = {}
    return HttpResponse(template.render(context, request))

def get_users_gender_tweets(request):
    all_users = GenderTwitterUser.objects.filter().order_by('identifier')

    female_tweets, male_tweets, brand_tweets = 0, 0, 0

    tmp_arr = [0, 0 , 0]
    for usr in all_users:
        tmp_arr[usr.gender] += int(usr.tweets)

    print(tmp_arr)

    data = []
    data.append({"label": 'Male', "value": tmp_arr[0]})
    data.append({"label": 'Female', "value": tmp_arr[1]})
    data.append({"label": 'Brand', "value": tmp_arr[2]})


    return JsonResponse(data, safe=False)

"""
Ordenados segun el indice de influencer
en plan, los 10 usuarios más influyentes
y el indice de influencer es
#seguidores / #siguiendo
siempre y cuando tenga mas de 1400 seguidores
se calcula ese cociente para todos los usuarios con mas de 1400 usuarios y se ordena
"""
def get_influencers(request):
    users = list(TwitterUser.objects.filter(followers__gt=1400).order_by('-followers'))
    
    influencers = []

    for user in users[0:10]:
        user_keys = vars(user)

        try:
            del user_keys["_state"]
        except KeyError:
            pass

        user_keys["ratio"] = round(user_keys["followers"] / user_keys["following"],2)

        influencers.append(user_keys);

    influencers.sort(key=lambda k: k["ratio"], reverse=True)

    for user_keys in influencers:
        print(type(user_keys["followers"]))
        print(user_keys["followers"])

    return JsonResponse(influencers, safe=False)

def wordclouds(request):
    template = loader.get_template('dashboard/wordclouds.html')
    context = {}
    return HttpResponse(template.render(context, request))
