# Create your models here.
from __future__ import unicode_literals

from django.db import models


class Play(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()


class TwitterUser(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    identifier = models.IntegerField(verbose_name='Identificador')
    creation_date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha creación')
    followers = models.IntegerField(verbose_name='Seguidores')
    following = models.IntegerField(verbose_name='Siguiendo')
    tweets = models.IntegerField(verbose_name='Tweets emitidos')
    favs = models.IntegerField(verbose_name='Favoritos')

    def __str__(self):
        return "{} - {}".format(self.identifier, self.name)

    @property
    def retweets_received(self):
        edges = Edge.objects.filter(retweeted=self)
        return edges.count()

    @property
    def retweets_performed(self):
        edges = Edge.objects.filter(retweeter=self)
        return edges.count()


class Edge(models.Model):
    retweeter = models.ForeignKey(TwitterUser, verbose_name='Retuiteador',
                                  on_delete=models.CASCADE, related_name='retweeter')
    retweeted = models.ForeignKey(TwitterUser, verbose_name='Retuiteado',
                                  on_delete=models.CASCADE, related_name='retweeted')

    def __str__(self):
        return "{} -> {}".format(self.retweeter, self.retweeted)


GENDER_MALE_INT = 0
GENDER_FEMALE_INT = 1
GENDER_BRAND_INT = 2
GENDER_CHOICES = (
    (GENDER_MALE_INT, 'Male'),
    (GENDER_FEMALE_INT, 'Female'),
    (GENDER_BRAND_INT, 'Brand'),
)


class GenderTwitterUser(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    identifier = models.IntegerField(verbose_name='Identificador')
    creation_date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha creación')
    tweets = models.IntegerField(verbose_name='Tweets emitidos')
    favs = models.IntegerField(verbose_name='Favoritos')
    gender = models.IntegerField(choices=GENDER_CHOICES)

    def __str__(self):
        return "{} - {}".format(self.identifier, self.name)

