import datetime
import math

import pandas as pd

from dashboard.models import TwitterUser, Edge, GenderTwitterUser, GENDER_MALE_INT, GENDER_FEMALE_INT, GENDER_BRAND_INT

NAME = 'Nombre'
ID = 'ID'
CREATION_DATE = 'Fecha creacion'
FOLLOWERS = 'Seguidores'
FOLLOWING = 'Siguiendo'
TWEETS = 'Tweets emitidos'
FAVS = 'Veces favorito'
GENDER = 'Genero'

RETWEETER = 'Retuiteador'
RETWEETED = 'Retuiteado'
ID_OWNER = 'Id_owner'
ID_RETWEETER = 'ID_retweeter'

GENDER_MALE_STR = 'male'
GENDER_FEMALE_STR = 'female'
GENDER_BRAND_STR = 'brand'


def ingest_nodes():
    df = pd.read_csv('dashboard/data/Nodos_preprocesados.csv')
    for index, user in df.iterrows():
        creation_date_str = user[CREATION_DATE]
        if creation_date_str == 'None':
            creation_date = None
        else:
            creation_date = datetime.datetime.strptime(creation_date_str, "%a %b %d %H:%M:%S %z %Y")

        tu = TwitterUser(name=user[NAME],
                         identifier=int(user[ID]),
                         creation_date=creation_date,
                         followers=int(user[FOLLOWERS]),
                         following=int(user[FOLLOWING]),
                         tweets=int(user[TWEETS]),
                         favs=int(user[FAVS]),
                         )
        tu.save()


def ingest_edges():
    df = pd.read_csv('dashboard/data/Arcos.csv')
    for index, edge in df.iterrows():
        retweeter_id = edge[ID_RETWEETER]
        retweeted_id = edge[ID_OWNER]

        if not math.isnan(retweeter_id) and not math.isnan(retweeted_id):
            retweeter_id = int(retweeter_id)
            retweeted_id = int(retweeted_id)

            try:
                retweeter = TwitterUser.objects.get(identifier=retweeter_id)
            except TwitterUser.MultipleObjectsReturned:
                retweeter = None
            except TwitterUser.DoesNotExist:
                retweeter = None

            if retweeter:
                try:
                    retweeted = TwitterUser.objects.get(identifier=retweeted_id)
                except TwitterUser.MultipleObjectsReturned:
                    retweeted = None
                except TwitterUser.DoesNotExist:
                    retweeted = None

                if retweeted:
                    e = Edge(retweeter=retweeter, retweeted=retweeted,)
                    e.save()


def ingest_gender_nodes():
    df = pd.read_csv('dashboard/data/gender_nodes_preprocessed.csv')
    for index, user in df.iterrows():
        creation_date_str = user[CREATION_DATE]
        if creation_date_str == 'None':
            creation_date = None
        else:
            creation_date = datetime.datetime.strptime(creation_date_str, "%d/%m/%Y %H:%M:%S")

        gender_str = user[GENDER]
        if gender_str == GENDER_MALE_STR:
            gender = GENDER_MALE_INT
        elif gender_str == GENDER_FEMALE_STR:
            gender = GENDER_FEMALE_INT
        elif gender_str == GENDER_BRAND_STR:
            gender = GENDER_BRAND_INT
        else:
            gender = None

        tu = GenderTwitterUser(name=user[NAME],
                               identifier=int(user[ID]),
                               creation_date=creation_date,
                               tweets=int(user[TWEETS]),
                               favs=int(user[FAVS]),
                               gender=gender
                               )
        tu.save()
