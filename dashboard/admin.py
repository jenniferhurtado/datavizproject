from django.contrib import admin


from .models import Play, TwitterUser, Edge, GenderTwitterUser

admin.site.register(Play)
admin.site.register(TwitterUser)
admin.site.register(Edge)
admin.site.register(GenderTwitterUser)
