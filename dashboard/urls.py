from django.conf.urls import url
from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('users_by_year', users_by_year, name='users_by_year'),
    path('force-graph/followers/<int:gt_followers>/', forceGraph, name='force-graph'),
    path('force-graph/fav/<int:gt_fav>/', forceGraph, name='force-graph'),
    path('force-graph', forceGraph, name='force-graph'),
    path('popularity_plot', popularity_plot, name='fpopularity_plot'),
    path('relationships', relationships, name='relationships'),
    path('genders_of_users', genders_of_users, name='genders_of_users'),
    path('gender_tweets', gender_tweets, name='gender_tweets'),
    path('influencers', influencers, name='influencers'),
    path('wordclouds', wordclouds, name='wordclouds'),
    url(r'^api/play_count_by_month', play_count_by_month, name='play_count_by_month'),
    url(r'^api/users_created_by_year', users_created_by_year, name='users_created_by_year'),
    url(r'^api/get_popularity_data', get_popularity_data, name='get_popularity_data'),
    url(r'^api/user_favorites', user_favorites, name='user_favorites'),
    url(r'^api/favorite_users', favorite_users, name='favorite_users'),
    url(r'^api/users_by_id',users_by_id,name='users_by_id'),
    url(r'^api/get_users_gender_data',get_users_gender_data,name='get_users_gender_data'),
    url(r'^api/get_users_gender_tweets',get_users_gender_tweets,name='get_users_gender_tweets'),
    url(r'^api/get_influencers',get_influencers,name='get_influencers')
    ]